import * as esbuild from 'esbuild'

await esbuild.build({
    entryPoints: ['src/index.ts'],
    outfile: 'dist/index.mjs',
    platform: "node",
    target: "node20",
    format: "esm",
    bundle: true,
    minify: true,
    // banner:js hack from https://github.com/evanw/esbuild/pull/2067
    banner: {
        "js": "import { createRequire } from 'module';const require = createRequire(import.meta.url);"
    },
    legalComments: "external",
    sourcemap: "external",
    allowOverwrite: true,
    external: [
        "@aws-sdk/*"
    ]
})
