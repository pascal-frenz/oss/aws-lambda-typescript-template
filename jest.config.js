/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  roots: ['src', 'test'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  coverageDirectory: 'coverage'
};
