# AWS Lambda Typescript Template
![Static Badge](https://img.shields.io/badge/status-actively%20maintained-3ea218?logo=gitlab)
![GitLab Issues](https://img.shields.io/gitlab/issues/open/55710403?logo=gitlab)
![GitLab Merge Requests](https://img.shields.io/gitlab/merge-requests/open/55710403?logo=gitlab)

This is a template to get you up and running quick with AWS Lambda best practices built in.

## Batteries Included
![Static Badge](https://img.shields.io/badge/npm-✅-grey?logo=npm)
![Static Badge](https://img.shields.io/badge/pnpm-✅-grey?logo=pnpm)
![Static Badge](https://img.shields.io/badge/yarn-✅-grey?logo=yarn)

Things are set up to get you going quickly. This is what you get:

* [Typescript](https://www.typescriptlang.org/)
* [Middy](https://github.com/middyjs/middy)
* [ESBuild](https://esbuild.github.io/)
* [ESLint](https://eslint.org/)
* [Prettier](https://prettier.io/)

## Installation
Download the files from the repository as a zip file and extract it where you want your project to live.

You can easily do this with `git archive`. Navigate into the directory where the files should go and run:

```shell
wget https://gitlab.com/pascal-frenz/oss/aws-lambda-typescript-template/-/archive/main/aws-lambda-typescript-template-main.zip -O temp.zip; \
unzip temp.zip; \
mv aws-lambda-typescript-template-main <your_project_name>; \
rm temp.zip;
```

Now you can start developing your lambda.

## Support
Just submit issues here via Gitlab.

## Contributing
Currently, I will only contribute myself. This might change in the future. I will update this section accordingly then.

## License
MIT License (see [LICENSE.md](LICENSE.md))
