import {LocalstackContainer, StartedLocalStackContainer} from "@testcontainers/localstack";
import {CreateBucketCommand, HeadBucketCommand, S3Client} from "@aws-sdk/client-s3";

describe('Infrastructure Test Suite (Testcontainers)', () => {
    // Since we work with docker containers here, things might take some time
    jest.setTimeout(60000);

    let container: StartedLocalStackContainer;
    let client: S3Client

    beforeAll(async () => {
        container = await new LocalstackContainer("localstack/localstack:3.2.0").start()
        client = new S3Client({
            endpoint: container.getConnectionUri(),
            forcePathStyle: true,
            region: "us-east-1",
            credentials: {
                secretAccessKey: "test",
                accessKeyId: "test"
            }
        })
    })

    it('should create s3 bucket', async () => {
        const input = {Bucket: "testcontainers"};
        const command = new CreateBucketCommand(input);

        const createBucketResponse = await client.send(command);
        expect(createBucketResponse.$metadata.httpStatusCode).toEqual(200);
        const headBucketResponse = await client.send(new HeadBucketCommand(input));
        expect(headBucketResponse.$metadata.httpStatusCode).toEqual(200);
    });

    afterAll(async () => {
        await container.stop()
    });
});
